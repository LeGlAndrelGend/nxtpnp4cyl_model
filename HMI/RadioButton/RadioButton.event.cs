/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/28/2014
 * Time: 11:33 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #RadioButton_HMI;

namespace HMI.Main.Symbols.RadioButton
{

  public class REQEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public REQEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_LabelText(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String LabelText
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,0, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }

    public bool Get_radio1Text(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String radio1Text
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,1, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }

    public bool Get_radio2Text(ref System.String value)
    {
      if (accessorService == null)
        return false;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,2, ref var);
      if (ret) value = (System.String) var;
      return ret;
    }

    public System.String radio2Text
    { get {
      if (accessorService == null)
        return null;
      string var = null;
      bool ret = accessorService.GetStringValue(channelId, cookie, eventIndex, true,2, ref var);
      if (!ret) return null;
      return (System.String) var;
    }  }


  }

}

namespace HMI.Main.Symbols.RadioButton
{

  public class ExtendEventArgs : System.EventArgs
  {
    public ExtendEventArgs()
    {
    }

  }

  public class RetractEventArgs : System.EventArgs
  {
    public RetractEventArgs()
    {
    }

  }

}

namespace HMI.Main.Symbols.RadioButton
{
  partial class sDefault
  {

    private event EventHandler<HMI.Main.Symbols.RadioButton.REQEventArgs> REQ_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.RadioButton.REQEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_Extend()
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {});
    }
    public bool FireEvent_Extend(HMI.Main.Symbols.RadioButton.ExtendEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_Retract()
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {});
    }
    public bool FireEvent_Retract(HMI.Main.Symbols.RadioButton.RetractEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }

  }
}

namespace HMI.Main.Symbols.RadioButton
{
  partial class RadioButtonGroup
  {

    private event EventHandler<HMI.Main.Symbols.RadioButton.REQEventArgs> REQ_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.RadioButton.REQEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_Extend()
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {});
    }
    public bool FireEvent_Extend(HMI.Main.Symbols.RadioButton.ExtendEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_Retract()
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {});
    }
    public bool FireEvent_Retract(HMI.Main.Symbols.RadioButton.RetractEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }

  }
}
#endregion #RadioButton_HMI;

#endregion Definitions;
