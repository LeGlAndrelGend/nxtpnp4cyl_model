﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/28/2014
 * Time: 11:33 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.RadioButton
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
	  public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		  this.REQ_Fired += new EventHandler<REQEventArgs>(onReqEvent);
		}
		void onReqEvent(object sender, REQEventArgs ea)
    { 
		  //System.Windows.Forms.MessageBox.Show("REQ!");
		}	  
	}
}
