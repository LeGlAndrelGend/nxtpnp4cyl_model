﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/28/2014
 * Time: 11:45 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.PnPManualControl
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.CYL1 = new HMI.Main.Symbols.RadioButton.sDefault();
			this.CYL2 = new HMI.Main.Symbols.RadioButton.sDefault();
			this.CYL3 = new HMI.Main.Symbols.RadioButton.sDefault();
			this.VCYL1 = new HMI.Main.Symbols.RadioButton.sDefault();
			this.VCYL2 = new HMI.Main.Symbols.RadioButton.sDefault();
			this.VCYL3 = new HMI.Main.Symbols.RadioButton.sDefault();
			this.Vaccum = new HMI.Main.Symbols.RadioButton.sDefault();
			// 
			// CYL1
			// 
			this.CYL1.BeginInit();
			this.CYL1.AngleIgnore = false;
			this.CYL1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 2, 2);
			this.CYL1.Name = "CYL1";
			this.CYL1.SecurityToken = ((uint)(4294967295u));
			this.CYL1.TagName = "CYL1";
			this.CYL1.EndInit();
			// 
			// CYL2
			// 
			this.CYL2.BeginInit();
			this.CYL2.AngleIgnore = false;
			this.CYL2.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 2, 28);
			this.CYL2.Name = "CYL2";
			this.CYL2.SecurityToken = ((uint)(4294967295u));
			this.CYL2.TagName = "CYL2";
			this.CYL2.EndInit();
			// 
			// CYL3
			// 
			this.CYL3.BeginInit();
			this.CYL3.AngleIgnore = false;
			this.CYL3.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 2, 52);
			this.CYL3.Name = "CYL3";
			this.CYL3.SecurityToken = ((uint)(4294967295u));
			this.CYL3.TagName = "CYL3";
			this.CYL3.EndInit();
			// 
			// VCYL1
			// 
			this.VCYL1.BeginInit();
			this.VCYL1.AngleIgnore = false;
			this.VCYL1.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 181, 3);
			this.VCYL1.Name = "VCYL1";
			this.VCYL1.SecurityToken = ((uint)(4294967295u));
			this.VCYL1.TagName = "VCYL1";
			this.VCYL1.EndInit();
			// 
			// VCYL2
			// 
			this.VCYL2.BeginInit();
			this.VCYL2.AngleIgnore = false;
			this.VCYL2.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 181, 28);
			this.VCYL2.Name = "VCYL2";
			this.VCYL2.SecurityToken = ((uint)(4294967295u));
			this.VCYL2.TagName = "VCYL2";
			this.VCYL2.EndInit();
			// 
			// VCYL3
			// 
			this.VCYL3.BeginInit();
			this.VCYL3.AngleIgnore = false;
			this.VCYL3.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 181, 52);
			this.VCYL3.Name = "VCYL3";
			this.VCYL3.SecurityToken = ((uint)(4294967295u));
			this.VCYL3.TagName = "VCYL3";
			this.VCYL3.EndInit();
			// 
			// Vaccum
			// 
			this.Vaccum.BeginInit();
			this.Vaccum.AngleIgnore = false;
			this.Vaccum.DesignTransformation = new NxtControl.Drawing.Matrix(1, 0, 0, 1, 98, 79);
			this.Vaccum.Name = "Vaccum";
			this.Vaccum.SecurityToken = ((uint)(4294967295u));
			this.Vaccum.TagName = "Vaccum";
			this.Vaccum.EndInit();
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.CYL1,
									this.CYL2,
									this.CYL3,
									this.VCYL1,
									this.VCYL2,
									this.VCYL3,
									this.Vaccum});
			this.SymbolSize = new System.Drawing.Size(394, 127);
		}
		private HMI.Main.Symbols.RadioButton.sDefault Vaccum;
		private HMI.Main.Symbols.RadioButton.sDefault VCYL3;
		private HMI.Main.Symbols.RadioButton.sDefault VCYL2;
		private HMI.Main.Symbols.RadioButton.sDefault VCYL1;
		private HMI.Main.Symbols.RadioButton.sDefault CYL3;
		private HMI.Main.Symbols.RadioButton.sDefault CYL2;
		private HMI.Main.Symbols.RadioButton.sDefault CYL1;
		#endregion
	}
}
