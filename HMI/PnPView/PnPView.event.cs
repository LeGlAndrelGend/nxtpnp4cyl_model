/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/5/2014
 * Time: 8:25 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #PnPView_HMI;

namespace HMI.Main.Symbols.PnPView
{

  public class REQEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public REQEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }
    public bool Get_BASE_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,0, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? BASE_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,0, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_BASE_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,1, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? BASE_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,1, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_CYL1_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,2, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? CYL1_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,2, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_CYL1_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,3, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? CYL1_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,3, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_CYL2_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,4, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? CYL2_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,4, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_CYL2_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,5, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? CYL2_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,5, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_CYL3_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,6, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? CYL3_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,6, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_CYL3_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,7, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? CYL3_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,7, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_VCYL1_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,8, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? VCYL1_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,8, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_VCYL1_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,9, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? VCYL1_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,9, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_VCYL2_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,10, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? VCYL2_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,10, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_VCYL2_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,11, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? VCYL2_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,11, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_VCYL3_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,12, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? VCYL3_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,12, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_VCYL3_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,13, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? VCYL3_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,13, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_WP1_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,14, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? WP1_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,14, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_WP1_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,15, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? WP1_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,15, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_WP2_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,16, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? WP2_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,16, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_WP2_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,17, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? WP2_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,17, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_WP3_X(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,18, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? WP3_X
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,18, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_WP3_Y(ref System.UInt16 value)
    {
      if (accessorService == null)
        return false;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,19, ref var);
      if (ret) value = (System.UInt16) var;
      return ret;
    }

    public System.UInt16? WP3_Y
    { get {
      if (accessorService == null)
        return null;
      System.Int64 var = 0;
      bool ret = accessorService.GetInt64Value(channelId, cookie, eventIndex, true,19, ref var);
      if (!ret) return null;
      return (System.UInt16) var;
    }  }

    public bool Get_VACUUM_ON(ref System.Boolean value)
    {
      if (accessorService == null)
        return false;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,20, ref var);
      if (ret) value = (System.Boolean) var;
      return ret;
    }

    public System.Boolean? VACUUM_ON
    { get {
      if (accessorService == null)
        return null;
      bool var = false;
      bool ret = accessorService.GetBoolValue(channelId, cookie, eventIndex, true,20, ref var);
      if (!ret) return null;
      return (System.Boolean) var;
    }  }


  }

  public class SET1EventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public SET1EventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

  public class SET2EventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public SET2EventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

  public class SET3EventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public SET3EventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

  public class SET4EventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public SET4EventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

  public class SET5EventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public SET5EventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

  public class SET6EventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public SET6EventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

  public class VerScheduledEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public VerScheduledEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

  public class HorScheduledEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public HorScheduledEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

  public class ResetEventArgs : System.EventArgs
  {
    IHMIAccessorService accessorService;
    int channelId;
    int cookie; 
    int eventIndex;

    public ResetEventArgs(int channelId, int cookie, int eventIndex)
    {
      this.accessorService = (IHMIAccessorService)ServiceProvider.GetService(typeof(IHMIAccessorService));
      this.channelId = channelId;
      this.cookie = cookie;
      this.eventIndex = eventIndex;
    }

  }

}

namespace HMI.Main.Symbols.PnPView
{

  public class CNFEventArgs : System.EventArgs
  {
    public CNFEventArgs()
    {
    }
    private System.Boolean? ADD1_field = null;
    public System.Boolean? ADD1
    {
       get { return ADD1_field; }
       set { ADD1_field = value; }
    }
    private System.Boolean? ADD2_field = null;
    public System.Boolean? ADD2
    {
       get { return ADD2_field; }
       set { ADD2_field = value; }
    }
    private System.Boolean? ADD3_field = null;
    public System.Boolean? ADD3
    {
       get { return ADD3_field; }
       set { ADD3_field = value; }
    }
    private System.UInt16? Xpos_field = null;
    public System.UInt16? Xpos
    {
       get { return Xpos_field; }
       set { Xpos_field = value; }
    }
    private System.UInt16? Ypos_field = null;
    public System.UInt16? Ypos
    {
       get { return Ypos_field; }
       set { Ypos_field = value; }
    }

  }

  public class MODEEventArgs : System.EventArgs
  {
    public MODEEventArgs()
    {
    }
    private System.SByte? Mode_field = null;
    public System.SByte? Mode
    {
       get { return Mode_field; }
       set { Mode_field = value; }
    }

  }

  public class ESTOPEventArgs : System.EventArgs
  {
    public ESTOPEventArgs()
    {
    }

  }

  public class HISTOPEventArgs : System.EventArgs
  {
    public HISTOPEventArgs()
    {
    }

  }

  public class FAULTEventArgs : System.EventArgs
  {
    public FAULTEventArgs()
    {
    }
    private System.Boolean? fCYL1_field = null;
    public System.Boolean? fCYL1
    {
       get { return fCYL1_field; }
       set { fCYL1_field = value; }
    }
    private System.Boolean? fCYL2_field = null;
    public System.Boolean? fCYL2
    {
       get { return fCYL2_field; }
       set { fCYL2_field = value; }
    }
    private System.Boolean? fCYL3_field = null;
    public System.Boolean? fCYL3
    {
       get { return fCYL3_field; }
       set { fCYL3_field = value; }
    }
    private System.Boolean? fVCYL1_field = null;
    public System.Boolean? fVCYL1
    {
       get { return fVCYL1_field; }
       set { fVCYL1_field = value; }
    }
    private System.Boolean? fVCYL2_field = null;
    public System.Boolean? fVCYL2
    {
       get { return fVCYL2_field; }
       set { fVCYL2_field = value; }
    }
    private System.Boolean? fVCYL3_field = null;
    public System.Boolean? fVCYL3
    {
       get { return fVCYL3_field; }
       set { fVCYL3_field = value; }
    }

  }

}

namespace HMI.Main.Symbols.PnPView
{
  partial class sDefault
  {

    private event EventHandler<HMI.Main.Symbols.PnPView.REQEventArgs> REQ_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs> SET1_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs> SET2_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs> SET3_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs> SET4_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs> SET5_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs> SET6_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs> VerScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs> HorScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs> Reset_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);
      if (SET1_Fired != null)
        AttachEventInput(1);
      if (SET2_Fired != null)
        AttachEventInput(2);
      if (SET3_Fired != null)
        AttachEventInput(3);
      if (SET4_Fired != null)
        AttachEventInput(4);
      if (SET5_Fired != null)
        AttachEventInput(5);
      if (SET6_Fired != null)
        AttachEventInput(6);
      if (VerScheduled_Fired != null)
        AttachEventInput(7);
      if (HorScheduled_Fired != null)
        AttachEventInput(8);
      if (Reset_Fired != null)
        AttachEventInput(9);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.PnPView.REQEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (SET1_Fired != null)
            SET1_Fired(this, new HMI.Main.Symbols.PnPView.SET1EventArgs(channelId, cookie, eventIndex));
        break; 
        case 2:
          if (SET2_Fired != null)
            SET2_Fired(this, new HMI.Main.Symbols.PnPView.SET2EventArgs(channelId, cookie, eventIndex));
        break; 
        case 3:
          if (SET3_Fired != null)
            SET3_Fired(this, new HMI.Main.Symbols.PnPView.SET3EventArgs(channelId, cookie, eventIndex));
        break; 
        case 4:
          if (SET4_Fired != null)
            SET4_Fired(this, new HMI.Main.Symbols.PnPView.SET4EventArgs(channelId, cookie, eventIndex));
        break; 
        case 5:
          if (SET5_Fired != null)
            SET5_Fired(this, new HMI.Main.Symbols.PnPView.SET5EventArgs(channelId, cookie, eventIndex));
        break; 
        case 6:
          if (SET6_Fired != null)
            SET6_Fired(this, new HMI.Main.Symbols.PnPView.SET6EventArgs(channelId, cookie, eventIndex));
        break; 
        case 7:
          if (VerScheduled_Fired != null)
            VerScheduled_Fired(this, new HMI.Main.Symbols.PnPView.VerScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 8:
          if (HorScheduled_Fired != null)
            HorScheduled_Fired(this, new HMI.Main.Symbols.PnPView.HorScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 9:
          if (Reset_Fired != null)
            Reset_Fired(this, new HMI.Main.Symbols.PnPView.ResetEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean ADD1, System.Boolean ADD2, System.Boolean ADD3, System.UInt16 Xpos, System.UInt16 Ypos)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {ADD1, ADD2, ADD3, Xpos, Ypos});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.PnPView.CNFEventArgs ea)
    {
      object[] _values_ = new object[5];
      if (ea.ADD1.HasValue) _values_[0] = ea.ADD1.Value;
      if (ea.ADD2.HasValue) _values_[1] = ea.ADD2.Value;
      if (ea.ADD3.HasValue) _values_[2] = ea.ADD3.Value;
      if (ea.Xpos.HasValue) _values_[3] = ea.Xpos.Value;
      if (ea.Ypos.HasValue) _values_[4] = ea.Ypos.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean ADD1, bool ignore_ADD1, System.Boolean ADD2, bool ignore_ADD2, System.Boolean ADD3, bool ignore_ADD3, System.UInt16 Xpos, bool ignore_Xpos, System.UInt16 Ypos, bool ignore_Ypos)
    {
      object[] _values_ = new object[5];
      if (!ignore_ADD1) _values_[0] = ADD1;
      if (!ignore_ADD2) _values_[1] = ADD2;
      if (!ignore_ADD3) _values_[2] = ADD3;
      if (!ignore_Xpos) _values_[3] = Xpos;
      if (!ignore_Ypos) _values_[4] = Ypos;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {Mode});
    }
    public bool FireEvent_MODE(HMI.Main.Symbols.PnPView.MODEEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.Mode.HasValue) _values_[0] = ea.Mode.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode, bool ignore_Mode)
    {
      object[] _values_ = new object[1];
      if (!ignore_Mode) _values_[0] = Mode;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_ESTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {});
    }
    public bool FireEvent_ESTOP(HMI.Main.Symbols.PnPView.ESTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_HISTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {});
    }
    public bool FireEvent_HISTOP(HMI.Main.Symbols.PnPView.HISTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, System.Boolean fCYL2, System.Boolean fCYL3, System.Boolean fVCYL1, System.Boolean fVCYL2, System.Boolean fVCYL3)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3});
    }
    public bool FireEvent_FAULT(HMI.Main.Symbols.PnPView.FAULTEventArgs ea)
    {
      object[] _values_ = new object[6];
      if (ea.fCYL1.HasValue) _values_[0] = ea.fCYL1.Value;
      if (ea.fCYL2.HasValue) _values_[1] = ea.fCYL2.Value;
      if (ea.fCYL3.HasValue) _values_[2] = ea.fCYL3.Value;
      if (ea.fVCYL1.HasValue) _values_[3] = ea.fVCYL1.Value;
      if (ea.fVCYL2.HasValue) _values_[4] = ea.fVCYL2.Value;
      if (ea.fVCYL3.HasValue) _values_[5] = ea.fVCYL3.Value;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, bool ignore_fCYL1, System.Boolean fCYL2, bool ignore_fCYL2, System.Boolean fCYL3, bool ignore_fCYL3, System.Boolean fVCYL1, bool ignore_fVCYL1, System.Boolean fVCYL2, bool ignore_fVCYL2, System.Boolean fVCYL3, bool ignore_fVCYL3)
    {
      object[] _values_ = new object[6];
      if (!ignore_fCYL1) _values_[0] = fCYL1;
      if (!ignore_fCYL2) _values_[1] = fCYL2;
      if (!ignore_fCYL3) _values_[2] = fCYL3;
      if (!ignore_fVCYL1) _values_[3] = fVCYL1;
      if (!ignore_fVCYL2) _values_[4] = fVCYL2;
      if (!ignore_fVCYL3) _values_[5] = fVCYL3;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}

namespace HMI.Main.Faceplates.PnPView
{
  partial class Faceplate
  {

    private event EventHandler<HMI.Main.Symbols.PnPView.REQEventArgs> REQ_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs> SET1_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs> SET2_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs> SET3_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs> SET4_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs> SET5_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs> SET6_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs> VerScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs> HorScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs> Reset_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);
      if (SET1_Fired != null)
        AttachEventInput(1);
      if (SET2_Fired != null)
        AttachEventInput(2);
      if (SET3_Fired != null)
        AttachEventInput(3);
      if (SET4_Fired != null)
        AttachEventInput(4);
      if (SET5_Fired != null)
        AttachEventInput(5);
      if (SET6_Fired != null)
        AttachEventInput(6);
      if (VerScheduled_Fired != null)
        AttachEventInput(7);
      if (HorScheduled_Fired != null)
        AttachEventInput(8);
      if (Reset_Fired != null)
        AttachEventInput(9);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.PnPView.REQEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (SET1_Fired != null)
            SET1_Fired(this, new HMI.Main.Symbols.PnPView.SET1EventArgs(channelId, cookie, eventIndex));
        break; 
        case 2:
          if (SET2_Fired != null)
            SET2_Fired(this, new HMI.Main.Symbols.PnPView.SET2EventArgs(channelId, cookie, eventIndex));
        break; 
        case 3:
          if (SET3_Fired != null)
            SET3_Fired(this, new HMI.Main.Symbols.PnPView.SET3EventArgs(channelId, cookie, eventIndex));
        break; 
        case 4:
          if (SET4_Fired != null)
            SET4_Fired(this, new HMI.Main.Symbols.PnPView.SET4EventArgs(channelId, cookie, eventIndex));
        break; 
        case 5:
          if (SET5_Fired != null)
            SET5_Fired(this, new HMI.Main.Symbols.PnPView.SET5EventArgs(channelId, cookie, eventIndex));
        break; 
        case 6:
          if (SET6_Fired != null)
            SET6_Fired(this, new HMI.Main.Symbols.PnPView.SET6EventArgs(channelId, cookie, eventIndex));
        break; 
        case 7:
          if (VerScheduled_Fired != null)
            VerScheduled_Fired(this, new HMI.Main.Symbols.PnPView.VerScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 8:
          if (HorScheduled_Fired != null)
            HorScheduled_Fired(this, new HMI.Main.Symbols.PnPView.HorScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 9:
          if (Reset_Fired != null)
            Reset_Fired(this, new HMI.Main.Symbols.PnPView.ResetEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean ADD1, System.Boolean ADD2, System.Boolean ADD3, System.UInt16 Xpos, System.UInt16 Ypos)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {ADD1, ADD2, ADD3, Xpos, Ypos});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.PnPView.CNFEventArgs ea)
    {
      object[] _values_ = new object[5];
      if (ea.ADD1.HasValue) _values_[0] = ea.ADD1.Value;
      if (ea.ADD2.HasValue) _values_[1] = ea.ADD2.Value;
      if (ea.ADD3.HasValue) _values_[2] = ea.ADD3.Value;
      if (ea.Xpos.HasValue) _values_[3] = ea.Xpos.Value;
      if (ea.Ypos.HasValue) _values_[4] = ea.Ypos.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean ADD1, bool ignore_ADD1, System.Boolean ADD2, bool ignore_ADD2, System.Boolean ADD3, bool ignore_ADD3, System.UInt16 Xpos, bool ignore_Xpos, System.UInt16 Ypos, bool ignore_Ypos)
    {
      object[] _values_ = new object[5];
      if (!ignore_ADD1) _values_[0] = ADD1;
      if (!ignore_ADD2) _values_[1] = ADD2;
      if (!ignore_ADD3) _values_[2] = ADD3;
      if (!ignore_Xpos) _values_[3] = Xpos;
      if (!ignore_Ypos) _values_[4] = Ypos;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {Mode});
    }
    public bool FireEvent_MODE(HMI.Main.Symbols.PnPView.MODEEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.Mode.HasValue) _values_[0] = ea.Mode.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode, bool ignore_Mode)
    {
      object[] _values_ = new object[1];
      if (!ignore_Mode) _values_[0] = Mode;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_ESTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {});
    }
    public bool FireEvent_ESTOP(HMI.Main.Symbols.PnPView.ESTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_HISTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {});
    }
    public bool FireEvent_HISTOP(HMI.Main.Symbols.PnPView.HISTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, System.Boolean fCYL2, System.Boolean fCYL3, System.Boolean fVCYL1, System.Boolean fVCYL2, System.Boolean fVCYL3)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3});
    }
    public bool FireEvent_FAULT(HMI.Main.Symbols.PnPView.FAULTEventArgs ea)
    {
      object[] _values_ = new object[6];
      if (ea.fCYL1.HasValue) _values_[0] = ea.fCYL1.Value;
      if (ea.fCYL2.HasValue) _values_[1] = ea.fCYL2.Value;
      if (ea.fCYL3.HasValue) _values_[2] = ea.fCYL3.Value;
      if (ea.fVCYL1.HasValue) _values_[3] = ea.fVCYL1.Value;
      if (ea.fVCYL2.HasValue) _values_[4] = ea.fVCYL2.Value;
      if (ea.fVCYL3.HasValue) _values_[5] = ea.fVCYL3.Value;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, bool ignore_fCYL1, System.Boolean fCYL2, bool ignore_fCYL2, System.Boolean fCYL3, bool ignore_fCYL3, System.Boolean fVCYL1, bool ignore_fVCYL1, System.Boolean fVCYL2, bool ignore_fVCYL2, System.Boolean fVCYL3, bool ignore_fVCYL3)
    {
      object[] _values_ = new object[6];
      if (!ignore_fCYL1) _values_[0] = fCYL1;
      if (!ignore_fCYL2) _values_[1] = fCYL2;
      if (!ignore_fCYL3) _values_[2] = fCYL3;
      if (!ignore_fVCYL1) _values_[3] = fVCYL1;
      if (!ignore_fVCYL2) _values_[4] = fVCYL2;
      if (!ignore_fVCYL3) _values_[5] = fVCYL3;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}

namespace HMI.Main.Symbols.PnPView
{
  partial class CYL1ManControl
  {

    private event EventHandler<HMI.Main.Symbols.PnPView.REQEventArgs> REQ_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs> SET1_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs> SET2_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs> SET3_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs> SET4_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs> SET5_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs> SET6_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs> VerScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs> HorScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs> Reset_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);
      if (SET1_Fired != null)
        AttachEventInput(1);
      if (SET2_Fired != null)
        AttachEventInput(2);
      if (SET3_Fired != null)
        AttachEventInput(3);
      if (SET4_Fired != null)
        AttachEventInput(4);
      if (SET5_Fired != null)
        AttachEventInput(5);
      if (SET6_Fired != null)
        AttachEventInput(6);
      if (VerScheduled_Fired != null)
        AttachEventInput(7);
      if (HorScheduled_Fired != null)
        AttachEventInput(8);
      if (Reset_Fired != null)
        AttachEventInput(9);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.PnPView.REQEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (SET1_Fired != null)
            SET1_Fired(this, new HMI.Main.Symbols.PnPView.SET1EventArgs(channelId, cookie, eventIndex));
        break; 
        case 2:
          if (SET2_Fired != null)
            SET2_Fired(this, new HMI.Main.Symbols.PnPView.SET2EventArgs(channelId, cookie, eventIndex));
        break; 
        case 3:
          if (SET3_Fired != null)
            SET3_Fired(this, new HMI.Main.Symbols.PnPView.SET3EventArgs(channelId, cookie, eventIndex));
        break; 
        case 4:
          if (SET4_Fired != null)
            SET4_Fired(this, new HMI.Main.Symbols.PnPView.SET4EventArgs(channelId, cookie, eventIndex));
        break; 
        case 5:
          if (SET5_Fired != null)
            SET5_Fired(this, new HMI.Main.Symbols.PnPView.SET5EventArgs(channelId, cookie, eventIndex));
        break; 
        case 6:
          if (SET6_Fired != null)
            SET6_Fired(this, new HMI.Main.Symbols.PnPView.SET6EventArgs(channelId, cookie, eventIndex));
        break; 
        case 7:
          if (VerScheduled_Fired != null)
            VerScheduled_Fired(this, new HMI.Main.Symbols.PnPView.VerScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 8:
          if (HorScheduled_Fired != null)
            HorScheduled_Fired(this, new HMI.Main.Symbols.PnPView.HorScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 9:
          if (Reset_Fired != null)
            Reset_Fired(this, new HMI.Main.Symbols.PnPView.ResetEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean ADD1, System.Boolean ADD2, System.Boolean ADD3, System.UInt16 Xpos, System.UInt16 Ypos)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {ADD1, ADD2, ADD3, Xpos, Ypos});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.PnPView.CNFEventArgs ea)
    {
      object[] _values_ = new object[5];
      if (ea.ADD1.HasValue) _values_[0] = ea.ADD1.Value;
      if (ea.ADD2.HasValue) _values_[1] = ea.ADD2.Value;
      if (ea.ADD3.HasValue) _values_[2] = ea.ADD3.Value;
      if (ea.Xpos.HasValue) _values_[3] = ea.Xpos.Value;
      if (ea.Ypos.HasValue) _values_[4] = ea.Ypos.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean ADD1, bool ignore_ADD1, System.Boolean ADD2, bool ignore_ADD2, System.Boolean ADD3, bool ignore_ADD3, System.UInt16 Xpos, bool ignore_Xpos, System.UInt16 Ypos, bool ignore_Ypos)
    {
      object[] _values_ = new object[5];
      if (!ignore_ADD1) _values_[0] = ADD1;
      if (!ignore_ADD2) _values_[1] = ADD2;
      if (!ignore_ADD3) _values_[2] = ADD3;
      if (!ignore_Xpos) _values_[3] = Xpos;
      if (!ignore_Ypos) _values_[4] = Ypos;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {Mode});
    }
    public bool FireEvent_MODE(HMI.Main.Symbols.PnPView.MODEEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.Mode.HasValue) _values_[0] = ea.Mode.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode, bool ignore_Mode)
    {
      object[] _values_ = new object[1];
      if (!ignore_Mode) _values_[0] = Mode;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_ESTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {});
    }
    public bool FireEvent_ESTOP(HMI.Main.Symbols.PnPView.ESTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_HISTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {});
    }
    public bool FireEvent_HISTOP(HMI.Main.Symbols.PnPView.HISTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, System.Boolean fCYL2, System.Boolean fCYL3, System.Boolean fVCYL1, System.Boolean fVCYL2, System.Boolean fVCYL3)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3});
    }
    public bool FireEvent_FAULT(HMI.Main.Symbols.PnPView.FAULTEventArgs ea)
    {
      object[] _values_ = new object[6];
      if (ea.fCYL1.HasValue) _values_[0] = ea.fCYL1.Value;
      if (ea.fCYL2.HasValue) _values_[1] = ea.fCYL2.Value;
      if (ea.fCYL3.HasValue) _values_[2] = ea.fCYL3.Value;
      if (ea.fVCYL1.HasValue) _values_[3] = ea.fVCYL1.Value;
      if (ea.fVCYL2.HasValue) _values_[4] = ea.fVCYL2.Value;
      if (ea.fVCYL3.HasValue) _values_[5] = ea.fVCYL3.Value;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, bool ignore_fCYL1, System.Boolean fCYL2, bool ignore_fCYL2, System.Boolean fCYL3, bool ignore_fCYL3, System.Boolean fVCYL1, bool ignore_fVCYL1, System.Boolean fVCYL2, bool ignore_fVCYL2, System.Boolean fVCYL3, bool ignore_fVCYL3)
    {
      object[] _values_ = new object[6];
      if (!ignore_fCYL1) _values_[0] = fCYL1;
      if (!ignore_fCYL2) _values_[1] = fCYL2;
      if (!ignore_fCYL3) _values_[2] = fCYL3;
      if (!ignore_fVCYL1) _values_[3] = fVCYL1;
      if (!ignore_fVCYL2) _values_[4] = fVCYL2;
      if (!ignore_fVCYL3) _values_[5] = fVCYL3;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}

namespace HMI.Main.Symbols.PnPView
{
  partial class CYL2ManControl
  {

    private event EventHandler<HMI.Main.Symbols.PnPView.REQEventArgs> REQ_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs> SET1_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs> SET2_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs> SET3_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs> SET4_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs> SET5_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs> SET6_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs> VerScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs> HorScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs> Reset_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);
      if (SET1_Fired != null)
        AttachEventInput(1);
      if (SET2_Fired != null)
        AttachEventInput(2);
      if (SET3_Fired != null)
        AttachEventInput(3);
      if (SET4_Fired != null)
        AttachEventInput(4);
      if (SET5_Fired != null)
        AttachEventInput(5);
      if (SET6_Fired != null)
        AttachEventInput(6);
      if (VerScheduled_Fired != null)
        AttachEventInput(7);
      if (HorScheduled_Fired != null)
        AttachEventInput(8);
      if (Reset_Fired != null)
        AttachEventInput(9);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.PnPView.REQEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (SET1_Fired != null)
            SET1_Fired(this, new HMI.Main.Symbols.PnPView.SET1EventArgs(channelId, cookie, eventIndex));
        break; 
        case 2:
          if (SET2_Fired != null)
            SET2_Fired(this, new HMI.Main.Symbols.PnPView.SET2EventArgs(channelId, cookie, eventIndex));
        break; 
        case 3:
          if (SET3_Fired != null)
            SET3_Fired(this, new HMI.Main.Symbols.PnPView.SET3EventArgs(channelId, cookie, eventIndex));
        break; 
        case 4:
          if (SET4_Fired != null)
            SET4_Fired(this, new HMI.Main.Symbols.PnPView.SET4EventArgs(channelId, cookie, eventIndex));
        break; 
        case 5:
          if (SET5_Fired != null)
            SET5_Fired(this, new HMI.Main.Symbols.PnPView.SET5EventArgs(channelId, cookie, eventIndex));
        break; 
        case 6:
          if (SET6_Fired != null)
            SET6_Fired(this, new HMI.Main.Symbols.PnPView.SET6EventArgs(channelId, cookie, eventIndex));
        break; 
        case 7:
          if (VerScheduled_Fired != null)
            VerScheduled_Fired(this, new HMI.Main.Symbols.PnPView.VerScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 8:
          if (HorScheduled_Fired != null)
            HorScheduled_Fired(this, new HMI.Main.Symbols.PnPView.HorScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 9:
          if (Reset_Fired != null)
            Reset_Fired(this, new HMI.Main.Symbols.PnPView.ResetEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean ADD1, System.Boolean ADD2, System.Boolean ADD3, System.UInt16 Xpos, System.UInt16 Ypos)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {ADD1, ADD2, ADD3, Xpos, Ypos});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.PnPView.CNFEventArgs ea)
    {
      object[] _values_ = new object[5];
      if (ea.ADD1.HasValue) _values_[0] = ea.ADD1.Value;
      if (ea.ADD2.HasValue) _values_[1] = ea.ADD2.Value;
      if (ea.ADD3.HasValue) _values_[2] = ea.ADD3.Value;
      if (ea.Xpos.HasValue) _values_[3] = ea.Xpos.Value;
      if (ea.Ypos.HasValue) _values_[4] = ea.Ypos.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean ADD1, bool ignore_ADD1, System.Boolean ADD2, bool ignore_ADD2, System.Boolean ADD3, bool ignore_ADD3, System.UInt16 Xpos, bool ignore_Xpos, System.UInt16 Ypos, bool ignore_Ypos)
    {
      object[] _values_ = new object[5];
      if (!ignore_ADD1) _values_[0] = ADD1;
      if (!ignore_ADD2) _values_[1] = ADD2;
      if (!ignore_ADD3) _values_[2] = ADD3;
      if (!ignore_Xpos) _values_[3] = Xpos;
      if (!ignore_Ypos) _values_[4] = Ypos;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {Mode});
    }
    public bool FireEvent_MODE(HMI.Main.Symbols.PnPView.MODEEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.Mode.HasValue) _values_[0] = ea.Mode.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode, bool ignore_Mode)
    {
      object[] _values_ = new object[1];
      if (!ignore_Mode) _values_[0] = Mode;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_ESTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {});
    }
    public bool FireEvent_ESTOP(HMI.Main.Symbols.PnPView.ESTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_HISTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {});
    }
    public bool FireEvent_HISTOP(HMI.Main.Symbols.PnPView.HISTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, System.Boolean fCYL2, System.Boolean fCYL3, System.Boolean fVCYL1, System.Boolean fVCYL2, System.Boolean fVCYL3)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3});
    }
    public bool FireEvent_FAULT(HMI.Main.Symbols.PnPView.FAULTEventArgs ea)
    {
      object[] _values_ = new object[6];
      if (ea.fCYL1.HasValue) _values_[0] = ea.fCYL1.Value;
      if (ea.fCYL2.HasValue) _values_[1] = ea.fCYL2.Value;
      if (ea.fCYL3.HasValue) _values_[2] = ea.fCYL3.Value;
      if (ea.fVCYL1.HasValue) _values_[3] = ea.fVCYL1.Value;
      if (ea.fVCYL2.HasValue) _values_[4] = ea.fVCYL2.Value;
      if (ea.fVCYL3.HasValue) _values_[5] = ea.fVCYL3.Value;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, bool ignore_fCYL1, System.Boolean fCYL2, bool ignore_fCYL2, System.Boolean fCYL3, bool ignore_fCYL3, System.Boolean fVCYL1, bool ignore_fVCYL1, System.Boolean fVCYL2, bool ignore_fVCYL2, System.Boolean fVCYL3, bool ignore_fVCYL3)
    {
      object[] _values_ = new object[6];
      if (!ignore_fCYL1) _values_[0] = fCYL1;
      if (!ignore_fCYL2) _values_[1] = fCYL2;
      if (!ignore_fCYL3) _values_[2] = fCYL3;
      if (!ignore_fVCYL1) _values_[3] = fVCYL1;
      if (!ignore_fVCYL2) _values_[4] = fVCYL2;
      if (!ignore_fVCYL3) _values_[5] = fVCYL3;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}

namespace HMI.Main.Symbols.PnPView
{
  partial class CYL3ManControl
  {

    private event EventHandler<HMI.Main.Symbols.PnPView.REQEventArgs> REQ_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs> SET1_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs> SET2_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs> SET3_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs> SET4_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs> SET5_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs> SET6_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs> VerScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs> HorScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs> Reset_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);
      if (SET1_Fired != null)
        AttachEventInput(1);
      if (SET2_Fired != null)
        AttachEventInput(2);
      if (SET3_Fired != null)
        AttachEventInput(3);
      if (SET4_Fired != null)
        AttachEventInput(4);
      if (SET5_Fired != null)
        AttachEventInput(5);
      if (SET6_Fired != null)
        AttachEventInput(6);
      if (VerScheduled_Fired != null)
        AttachEventInput(7);
      if (HorScheduled_Fired != null)
        AttachEventInput(8);
      if (Reset_Fired != null)
        AttachEventInput(9);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.PnPView.REQEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (SET1_Fired != null)
            SET1_Fired(this, new HMI.Main.Symbols.PnPView.SET1EventArgs(channelId, cookie, eventIndex));
        break; 
        case 2:
          if (SET2_Fired != null)
            SET2_Fired(this, new HMI.Main.Symbols.PnPView.SET2EventArgs(channelId, cookie, eventIndex));
        break; 
        case 3:
          if (SET3_Fired != null)
            SET3_Fired(this, new HMI.Main.Symbols.PnPView.SET3EventArgs(channelId, cookie, eventIndex));
        break; 
        case 4:
          if (SET4_Fired != null)
            SET4_Fired(this, new HMI.Main.Symbols.PnPView.SET4EventArgs(channelId, cookie, eventIndex));
        break; 
        case 5:
          if (SET5_Fired != null)
            SET5_Fired(this, new HMI.Main.Symbols.PnPView.SET5EventArgs(channelId, cookie, eventIndex));
        break; 
        case 6:
          if (SET6_Fired != null)
            SET6_Fired(this, new HMI.Main.Symbols.PnPView.SET6EventArgs(channelId, cookie, eventIndex));
        break; 
        case 7:
          if (VerScheduled_Fired != null)
            VerScheduled_Fired(this, new HMI.Main.Symbols.PnPView.VerScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 8:
          if (HorScheduled_Fired != null)
            HorScheduled_Fired(this, new HMI.Main.Symbols.PnPView.HorScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 9:
          if (Reset_Fired != null)
            Reset_Fired(this, new HMI.Main.Symbols.PnPView.ResetEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean ADD1, System.Boolean ADD2, System.Boolean ADD3, System.UInt16 Xpos, System.UInt16 Ypos)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {ADD1, ADD2, ADD3, Xpos, Ypos});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.PnPView.CNFEventArgs ea)
    {
      object[] _values_ = new object[5];
      if (ea.ADD1.HasValue) _values_[0] = ea.ADD1.Value;
      if (ea.ADD2.HasValue) _values_[1] = ea.ADD2.Value;
      if (ea.ADD3.HasValue) _values_[2] = ea.ADD3.Value;
      if (ea.Xpos.HasValue) _values_[3] = ea.Xpos.Value;
      if (ea.Ypos.HasValue) _values_[4] = ea.Ypos.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean ADD1, bool ignore_ADD1, System.Boolean ADD2, bool ignore_ADD2, System.Boolean ADD3, bool ignore_ADD3, System.UInt16 Xpos, bool ignore_Xpos, System.UInt16 Ypos, bool ignore_Ypos)
    {
      object[] _values_ = new object[5];
      if (!ignore_ADD1) _values_[0] = ADD1;
      if (!ignore_ADD2) _values_[1] = ADD2;
      if (!ignore_ADD3) _values_[2] = ADD3;
      if (!ignore_Xpos) _values_[3] = Xpos;
      if (!ignore_Ypos) _values_[4] = Ypos;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {Mode});
    }
    public bool FireEvent_MODE(HMI.Main.Symbols.PnPView.MODEEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.Mode.HasValue) _values_[0] = ea.Mode.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode, bool ignore_Mode)
    {
      object[] _values_ = new object[1];
      if (!ignore_Mode) _values_[0] = Mode;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_ESTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {});
    }
    public bool FireEvent_ESTOP(HMI.Main.Symbols.PnPView.ESTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_HISTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {});
    }
    public bool FireEvent_HISTOP(HMI.Main.Symbols.PnPView.HISTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, System.Boolean fCYL2, System.Boolean fCYL3, System.Boolean fVCYL1, System.Boolean fVCYL2, System.Boolean fVCYL3)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3});
    }
    public bool FireEvent_FAULT(HMI.Main.Symbols.PnPView.FAULTEventArgs ea)
    {
      object[] _values_ = new object[6];
      if (ea.fCYL1.HasValue) _values_[0] = ea.fCYL1.Value;
      if (ea.fCYL2.HasValue) _values_[1] = ea.fCYL2.Value;
      if (ea.fCYL3.HasValue) _values_[2] = ea.fCYL3.Value;
      if (ea.fVCYL1.HasValue) _values_[3] = ea.fVCYL1.Value;
      if (ea.fVCYL2.HasValue) _values_[4] = ea.fVCYL2.Value;
      if (ea.fVCYL3.HasValue) _values_[5] = ea.fVCYL3.Value;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, bool ignore_fCYL1, System.Boolean fCYL2, bool ignore_fCYL2, System.Boolean fCYL3, bool ignore_fCYL3, System.Boolean fVCYL1, bool ignore_fVCYL1, System.Boolean fVCYL2, bool ignore_fVCYL2, System.Boolean fVCYL3, bool ignore_fVCYL3)
    {
      object[] _values_ = new object[6];
      if (!ignore_fCYL1) _values_[0] = fCYL1;
      if (!ignore_fCYL2) _values_[1] = fCYL2;
      if (!ignore_fCYL3) _values_[2] = fCYL3;
      if (!ignore_fVCYL1) _values_[3] = fVCYL1;
      if (!ignore_fVCYL2) _values_[4] = fVCYL2;
      if (!ignore_fVCYL3) _values_[5] = fVCYL3;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}

namespace HMI.Main.Symbols.PnPView
{
  partial class VCYL1ManControl
  {

    private event EventHandler<HMI.Main.Symbols.PnPView.REQEventArgs> REQ_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs> SET1_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs> SET2_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs> SET3_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs> SET4_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs> SET5_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs> SET6_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs> VerScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs> HorScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs> Reset_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);
      if (SET1_Fired != null)
        AttachEventInput(1);
      if (SET2_Fired != null)
        AttachEventInput(2);
      if (SET3_Fired != null)
        AttachEventInput(3);
      if (SET4_Fired != null)
        AttachEventInput(4);
      if (SET5_Fired != null)
        AttachEventInput(5);
      if (SET6_Fired != null)
        AttachEventInput(6);
      if (VerScheduled_Fired != null)
        AttachEventInput(7);
      if (HorScheduled_Fired != null)
        AttachEventInput(8);
      if (Reset_Fired != null)
        AttachEventInput(9);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.PnPView.REQEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (SET1_Fired != null)
            SET1_Fired(this, new HMI.Main.Symbols.PnPView.SET1EventArgs(channelId, cookie, eventIndex));
        break; 
        case 2:
          if (SET2_Fired != null)
            SET2_Fired(this, new HMI.Main.Symbols.PnPView.SET2EventArgs(channelId, cookie, eventIndex));
        break; 
        case 3:
          if (SET3_Fired != null)
            SET3_Fired(this, new HMI.Main.Symbols.PnPView.SET3EventArgs(channelId, cookie, eventIndex));
        break; 
        case 4:
          if (SET4_Fired != null)
            SET4_Fired(this, new HMI.Main.Symbols.PnPView.SET4EventArgs(channelId, cookie, eventIndex));
        break; 
        case 5:
          if (SET5_Fired != null)
            SET5_Fired(this, new HMI.Main.Symbols.PnPView.SET5EventArgs(channelId, cookie, eventIndex));
        break; 
        case 6:
          if (SET6_Fired != null)
            SET6_Fired(this, new HMI.Main.Symbols.PnPView.SET6EventArgs(channelId, cookie, eventIndex));
        break; 
        case 7:
          if (VerScheduled_Fired != null)
            VerScheduled_Fired(this, new HMI.Main.Symbols.PnPView.VerScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 8:
          if (HorScheduled_Fired != null)
            HorScheduled_Fired(this, new HMI.Main.Symbols.PnPView.HorScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 9:
          if (Reset_Fired != null)
            Reset_Fired(this, new HMI.Main.Symbols.PnPView.ResetEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean ADD1, System.Boolean ADD2, System.Boolean ADD3, System.UInt16 Xpos, System.UInt16 Ypos)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {ADD1, ADD2, ADD3, Xpos, Ypos});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.PnPView.CNFEventArgs ea)
    {
      object[] _values_ = new object[5];
      if (ea.ADD1.HasValue) _values_[0] = ea.ADD1.Value;
      if (ea.ADD2.HasValue) _values_[1] = ea.ADD2.Value;
      if (ea.ADD3.HasValue) _values_[2] = ea.ADD3.Value;
      if (ea.Xpos.HasValue) _values_[3] = ea.Xpos.Value;
      if (ea.Ypos.HasValue) _values_[4] = ea.Ypos.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean ADD1, bool ignore_ADD1, System.Boolean ADD2, bool ignore_ADD2, System.Boolean ADD3, bool ignore_ADD3, System.UInt16 Xpos, bool ignore_Xpos, System.UInt16 Ypos, bool ignore_Ypos)
    {
      object[] _values_ = new object[5];
      if (!ignore_ADD1) _values_[0] = ADD1;
      if (!ignore_ADD2) _values_[1] = ADD2;
      if (!ignore_ADD3) _values_[2] = ADD3;
      if (!ignore_Xpos) _values_[3] = Xpos;
      if (!ignore_Ypos) _values_[4] = Ypos;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {Mode});
    }
    public bool FireEvent_MODE(HMI.Main.Symbols.PnPView.MODEEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.Mode.HasValue) _values_[0] = ea.Mode.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode, bool ignore_Mode)
    {
      object[] _values_ = new object[1];
      if (!ignore_Mode) _values_[0] = Mode;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_ESTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {});
    }
    public bool FireEvent_ESTOP(HMI.Main.Symbols.PnPView.ESTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_HISTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {});
    }
    public bool FireEvent_HISTOP(HMI.Main.Symbols.PnPView.HISTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, System.Boolean fCYL2, System.Boolean fCYL3, System.Boolean fVCYL1, System.Boolean fVCYL2, System.Boolean fVCYL3)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3});
    }
    public bool FireEvent_FAULT(HMI.Main.Symbols.PnPView.FAULTEventArgs ea)
    {
      object[] _values_ = new object[6];
      if (ea.fCYL1.HasValue) _values_[0] = ea.fCYL1.Value;
      if (ea.fCYL2.HasValue) _values_[1] = ea.fCYL2.Value;
      if (ea.fCYL3.HasValue) _values_[2] = ea.fCYL3.Value;
      if (ea.fVCYL1.HasValue) _values_[3] = ea.fVCYL1.Value;
      if (ea.fVCYL2.HasValue) _values_[4] = ea.fVCYL2.Value;
      if (ea.fVCYL3.HasValue) _values_[5] = ea.fVCYL3.Value;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, bool ignore_fCYL1, System.Boolean fCYL2, bool ignore_fCYL2, System.Boolean fCYL3, bool ignore_fCYL3, System.Boolean fVCYL1, bool ignore_fVCYL1, System.Boolean fVCYL2, bool ignore_fVCYL2, System.Boolean fVCYL3, bool ignore_fVCYL3)
    {
      object[] _values_ = new object[6];
      if (!ignore_fCYL1) _values_[0] = fCYL1;
      if (!ignore_fCYL2) _values_[1] = fCYL2;
      if (!ignore_fCYL3) _values_[2] = fCYL3;
      if (!ignore_fVCYL1) _values_[3] = fVCYL1;
      if (!ignore_fVCYL2) _values_[4] = fVCYL2;
      if (!ignore_fVCYL3) _values_[5] = fVCYL3;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}

namespace HMI.Main.Symbols.PnPView
{
  partial class VCYL2ManControl
  {

    private event EventHandler<HMI.Main.Symbols.PnPView.REQEventArgs> REQ_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs> SET1_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs> SET2_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs> SET3_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs> SET4_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs> SET5_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs> SET6_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs> VerScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs> HorScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs> Reset_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);
      if (SET1_Fired != null)
        AttachEventInput(1);
      if (SET2_Fired != null)
        AttachEventInput(2);
      if (SET3_Fired != null)
        AttachEventInput(3);
      if (SET4_Fired != null)
        AttachEventInput(4);
      if (SET5_Fired != null)
        AttachEventInput(5);
      if (SET6_Fired != null)
        AttachEventInput(6);
      if (VerScheduled_Fired != null)
        AttachEventInput(7);
      if (HorScheduled_Fired != null)
        AttachEventInput(8);
      if (Reset_Fired != null)
        AttachEventInput(9);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.PnPView.REQEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (SET1_Fired != null)
            SET1_Fired(this, new HMI.Main.Symbols.PnPView.SET1EventArgs(channelId, cookie, eventIndex));
        break; 
        case 2:
          if (SET2_Fired != null)
            SET2_Fired(this, new HMI.Main.Symbols.PnPView.SET2EventArgs(channelId, cookie, eventIndex));
        break; 
        case 3:
          if (SET3_Fired != null)
            SET3_Fired(this, new HMI.Main.Symbols.PnPView.SET3EventArgs(channelId, cookie, eventIndex));
        break; 
        case 4:
          if (SET4_Fired != null)
            SET4_Fired(this, new HMI.Main.Symbols.PnPView.SET4EventArgs(channelId, cookie, eventIndex));
        break; 
        case 5:
          if (SET5_Fired != null)
            SET5_Fired(this, new HMI.Main.Symbols.PnPView.SET5EventArgs(channelId, cookie, eventIndex));
        break; 
        case 6:
          if (SET6_Fired != null)
            SET6_Fired(this, new HMI.Main.Symbols.PnPView.SET6EventArgs(channelId, cookie, eventIndex));
        break; 
        case 7:
          if (VerScheduled_Fired != null)
            VerScheduled_Fired(this, new HMI.Main.Symbols.PnPView.VerScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 8:
          if (HorScheduled_Fired != null)
            HorScheduled_Fired(this, new HMI.Main.Symbols.PnPView.HorScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 9:
          if (Reset_Fired != null)
            Reset_Fired(this, new HMI.Main.Symbols.PnPView.ResetEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean ADD1, System.Boolean ADD2, System.Boolean ADD3, System.UInt16 Xpos, System.UInt16 Ypos)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {ADD1, ADD2, ADD3, Xpos, Ypos});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.PnPView.CNFEventArgs ea)
    {
      object[] _values_ = new object[5];
      if (ea.ADD1.HasValue) _values_[0] = ea.ADD1.Value;
      if (ea.ADD2.HasValue) _values_[1] = ea.ADD2.Value;
      if (ea.ADD3.HasValue) _values_[2] = ea.ADD3.Value;
      if (ea.Xpos.HasValue) _values_[3] = ea.Xpos.Value;
      if (ea.Ypos.HasValue) _values_[4] = ea.Ypos.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean ADD1, bool ignore_ADD1, System.Boolean ADD2, bool ignore_ADD2, System.Boolean ADD3, bool ignore_ADD3, System.UInt16 Xpos, bool ignore_Xpos, System.UInt16 Ypos, bool ignore_Ypos)
    {
      object[] _values_ = new object[5];
      if (!ignore_ADD1) _values_[0] = ADD1;
      if (!ignore_ADD2) _values_[1] = ADD2;
      if (!ignore_ADD3) _values_[2] = ADD3;
      if (!ignore_Xpos) _values_[3] = Xpos;
      if (!ignore_Ypos) _values_[4] = Ypos;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {Mode});
    }
    public bool FireEvent_MODE(HMI.Main.Symbols.PnPView.MODEEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.Mode.HasValue) _values_[0] = ea.Mode.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode, bool ignore_Mode)
    {
      object[] _values_ = new object[1];
      if (!ignore_Mode) _values_[0] = Mode;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_ESTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {});
    }
    public bool FireEvent_ESTOP(HMI.Main.Symbols.PnPView.ESTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_HISTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {});
    }
    public bool FireEvent_HISTOP(HMI.Main.Symbols.PnPView.HISTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, System.Boolean fCYL2, System.Boolean fCYL3, System.Boolean fVCYL1, System.Boolean fVCYL2, System.Boolean fVCYL3)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3});
    }
    public bool FireEvent_FAULT(HMI.Main.Symbols.PnPView.FAULTEventArgs ea)
    {
      object[] _values_ = new object[6];
      if (ea.fCYL1.HasValue) _values_[0] = ea.fCYL1.Value;
      if (ea.fCYL2.HasValue) _values_[1] = ea.fCYL2.Value;
      if (ea.fCYL3.HasValue) _values_[2] = ea.fCYL3.Value;
      if (ea.fVCYL1.HasValue) _values_[3] = ea.fVCYL1.Value;
      if (ea.fVCYL2.HasValue) _values_[4] = ea.fVCYL2.Value;
      if (ea.fVCYL3.HasValue) _values_[5] = ea.fVCYL3.Value;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, bool ignore_fCYL1, System.Boolean fCYL2, bool ignore_fCYL2, System.Boolean fCYL3, bool ignore_fCYL3, System.Boolean fVCYL1, bool ignore_fVCYL1, System.Boolean fVCYL2, bool ignore_fVCYL2, System.Boolean fVCYL3, bool ignore_fVCYL3)
    {
      object[] _values_ = new object[6];
      if (!ignore_fCYL1) _values_[0] = fCYL1;
      if (!ignore_fCYL2) _values_[1] = fCYL2;
      if (!ignore_fCYL3) _values_[2] = fCYL3;
      if (!ignore_fVCYL1) _values_[3] = fVCYL1;
      if (!ignore_fVCYL2) _values_[4] = fVCYL2;
      if (!ignore_fVCYL3) _values_[5] = fVCYL3;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}

namespace HMI.Main.Symbols.PnPView
{
  partial class VCYL3ManControl
  {

    private event EventHandler<HMI.Main.Symbols.PnPView.REQEventArgs> REQ_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs> SET1_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs> SET2_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs> SET3_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs> SET4_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs> SET5_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs> SET6_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs> VerScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs> HorScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs> Reset_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);
      if (SET1_Fired != null)
        AttachEventInput(1);
      if (SET2_Fired != null)
        AttachEventInput(2);
      if (SET3_Fired != null)
        AttachEventInput(3);
      if (SET4_Fired != null)
        AttachEventInput(4);
      if (SET5_Fired != null)
        AttachEventInput(5);
      if (SET6_Fired != null)
        AttachEventInput(6);
      if (VerScheduled_Fired != null)
        AttachEventInput(7);
      if (HorScheduled_Fired != null)
        AttachEventInput(8);
      if (Reset_Fired != null)
        AttachEventInput(9);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.PnPView.REQEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (SET1_Fired != null)
            SET1_Fired(this, new HMI.Main.Symbols.PnPView.SET1EventArgs(channelId, cookie, eventIndex));
        break; 
        case 2:
          if (SET2_Fired != null)
            SET2_Fired(this, new HMI.Main.Symbols.PnPView.SET2EventArgs(channelId, cookie, eventIndex));
        break; 
        case 3:
          if (SET3_Fired != null)
            SET3_Fired(this, new HMI.Main.Symbols.PnPView.SET3EventArgs(channelId, cookie, eventIndex));
        break; 
        case 4:
          if (SET4_Fired != null)
            SET4_Fired(this, new HMI.Main.Symbols.PnPView.SET4EventArgs(channelId, cookie, eventIndex));
        break; 
        case 5:
          if (SET5_Fired != null)
            SET5_Fired(this, new HMI.Main.Symbols.PnPView.SET5EventArgs(channelId, cookie, eventIndex));
        break; 
        case 6:
          if (SET6_Fired != null)
            SET6_Fired(this, new HMI.Main.Symbols.PnPView.SET6EventArgs(channelId, cookie, eventIndex));
        break; 
        case 7:
          if (VerScheduled_Fired != null)
            VerScheduled_Fired(this, new HMI.Main.Symbols.PnPView.VerScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 8:
          if (HorScheduled_Fired != null)
            HorScheduled_Fired(this, new HMI.Main.Symbols.PnPView.HorScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 9:
          if (Reset_Fired != null)
            Reset_Fired(this, new HMI.Main.Symbols.PnPView.ResetEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean ADD1, System.Boolean ADD2, System.Boolean ADD3, System.UInt16 Xpos, System.UInt16 Ypos)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {ADD1, ADD2, ADD3, Xpos, Ypos});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.PnPView.CNFEventArgs ea)
    {
      object[] _values_ = new object[5];
      if (ea.ADD1.HasValue) _values_[0] = ea.ADD1.Value;
      if (ea.ADD2.HasValue) _values_[1] = ea.ADD2.Value;
      if (ea.ADD3.HasValue) _values_[2] = ea.ADD3.Value;
      if (ea.Xpos.HasValue) _values_[3] = ea.Xpos.Value;
      if (ea.Ypos.HasValue) _values_[4] = ea.Ypos.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean ADD1, bool ignore_ADD1, System.Boolean ADD2, bool ignore_ADD2, System.Boolean ADD3, bool ignore_ADD3, System.UInt16 Xpos, bool ignore_Xpos, System.UInt16 Ypos, bool ignore_Ypos)
    {
      object[] _values_ = new object[5];
      if (!ignore_ADD1) _values_[0] = ADD1;
      if (!ignore_ADD2) _values_[1] = ADD2;
      if (!ignore_ADD3) _values_[2] = ADD3;
      if (!ignore_Xpos) _values_[3] = Xpos;
      if (!ignore_Ypos) _values_[4] = Ypos;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {Mode});
    }
    public bool FireEvent_MODE(HMI.Main.Symbols.PnPView.MODEEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.Mode.HasValue) _values_[0] = ea.Mode.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode, bool ignore_Mode)
    {
      object[] _values_ = new object[1];
      if (!ignore_Mode) _values_[0] = Mode;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_ESTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {});
    }
    public bool FireEvent_ESTOP(HMI.Main.Symbols.PnPView.ESTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_HISTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {});
    }
    public bool FireEvent_HISTOP(HMI.Main.Symbols.PnPView.HISTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, System.Boolean fCYL2, System.Boolean fCYL3, System.Boolean fVCYL1, System.Boolean fVCYL2, System.Boolean fVCYL3)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3});
    }
    public bool FireEvent_FAULT(HMI.Main.Symbols.PnPView.FAULTEventArgs ea)
    {
      object[] _values_ = new object[6];
      if (ea.fCYL1.HasValue) _values_[0] = ea.fCYL1.Value;
      if (ea.fCYL2.HasValue) _values_[1] = ea.fCYL2.Value;
      if (ea.fCYL3.HasValue) _values_[2] = ea.fCYL3.Value;
      if (ea.fVCYL1.HasValue) _values_[3] = ea.fVCYL1.Value;
      if (ea.fVCYL2.HasValue) _values_[4] = ea.fVCYL2.Value;
      if (ea.fVCYL3.HasValue) _values_[5] = ea.fVCYL3.Value;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, bool ignore_fCYL1, System.Boolean fCYL2, bool ignore_fCYL2, System.Boolean fCYL3, bool ignore_fCYL3, System.Boolean fVCYL1, bool ignore_fVCYL1, System.Boolean fVCYL2, bool ignore_fVCYL2, System.Boolean fVCYL3, bool ignore_fVCYL3)
    {
      object[] _values_ = new object[6];
      if (!ignore_fCYL1) _values_[0] = fCYL1;
      if (!ignore_fCYL2) _values_[1] = fCYL2;
      if (!ignore_fCYL3) _values_[2] = fCYL3;
      if (!ignore_fVCYL1) _values_[3] = fVCYL1;
      if (!ignore_fVCYL2) _values_[4] = fVCYL2;
      if (!ignore_fVCYL3) _values_[5] = fVCYL3;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}

namespace HMI.Main.Symbols.PnPView
{
  partial class VaccumManControl
  {

    private event EventHandler<HMI.Main.Symbols.PnPView.REQEventArgs> REQ_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET1EventArgs> SET1_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET2EventArgs> SET2_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET3EventArgs> SET3_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET4EventArgs> SET4_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET5EventArgs> SET5_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.SET6EventArgs> SET6_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.VerScheduledEventArgs> VerScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.HorScheduledEventArgs> HorScheduled_Fired;

    private event EventHandler<HMI.Main.Symbols.PnPView.ResetEventArgs> Reset_Fired;

    protected override void OnEndInit()
    {
      if (REQ_Fired != null)
        AttachEventInput(0);
      if (SET1_Fired != null)
        AttachEventInput(1);
      if (SET2_Fired != null)
        AttachEventInput(2);
      if (SET3_Fired != null)
        AttachEventInput(3);
      if (SET4_Fired != null)
        AttachEventInput(4);
      if (SET5_Fired != null)
        AttachEventInput(5);
      if (SET6_Fired != null)
        AttachEventInput(6);
      if (VerScheduled_Fired != null)
        AttachEventInput(7);
      if (HorScheduled_Fired != null)
        AttachEventInput(8);
      if (Reset_Fired != null)
        AttachEventInput(9);

    }

    protected override void FireEventCallback(int channelId, int cookie, int eventIndex)
    {
      switch(eventIndex)
      {
        default:
          break;
        case 0:
          if (REQ_Fired != null)
            REQ_Fired(this, new HMI.Main.Symbols.PnPView.REQEventArgs(channelId, cookie, eventIndex));
        break; 
        case 1:
          if (SET1_Fired != null)
            SET1_Fired(this, new HMI.Main.Symbols.PnPView.SET1EventArgs(channelId, cookie, eventIndex));
        break; 
        case 2:
          if (SET2_Fired != null)
            SET2_Fired(this, new HMI.Main.Symbols.PnPView.SET2EventArgs(channelId, cookie, eventIndex));
        break; 
        case 3:
          if (SET3_Fired != null)
            SET3_Fired(this, new HMI.Main.Symbols.PnPView.SET3EventArgs(channelId, cookie, eventIndex));
        break; 
        case 4:
          if (SET4_Fired != null)
            SET4_Fired(this, new HMI.Main.Symbols.PnPView.SET4EventArgs(channelId, cookie, eventIndex));
        break; 
        case 5:
          if (SET5_Fired != null)
            SET5_Fired(this, new HMI.Main.Symbols.PnPView.SET5EventArgs(channelId, cookie, eventIndex));
        break; 
        case 6:
          if (SET6_Fired != null)
            SET6_Fired(this, new HMI.Main.Symbols.PnPView.SET6EventArgs(channelId, cookie, eventIndex));
        break; 
        case 7:
          if (VerScheduled_Fired != null)
            VerScheduled_Fired(this, new HMI.Main.Symbols.PnPView.VerScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 8:
          if (HorScheduled_Fired != null)
            HorScheduled_Fired(this, new HMI.Main.Symbols.PnPView.HorScheduledEventArgs(channelId, cookie, eventIndex));
        break; 
        case 9:
          if (Reset_Fired != null)
            Reset_Fired(this, new HMI.Main.Symbols.PnPView.ResetEventArgs(channelId, cookie, eventIndex));
        break; 

      }
    }
    public bool FireEvent_CNF(System.Boolean ADD1, System.Boolean ADD2, System.Boolean ADD3, System.UInt16 Xpos, System.UInt16 Ypos)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {ADD1, ADD2, ADD3, Xpos, Ypos});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.PnPView.CNFEventArgs ea)
    {
      object[] _values_ = new object[5];
      if (ea.ADD1.HasValue) _values_[0] = ea.ADD1.Value;
      if (ea.ADD2.HasValue) _values_[1] = ea.ADD2.Value;
      if (ea.ADD3.HasValue) _values_[2] = ea.ADD3.Value;
      if (ea.Xpos.HasValue) _values_[3] = ea.Xpos.Value;
      if (ea.Ypos.HasValue) _values_[4] = ea.Ypos.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean ADD1, bool ignore_ADD1, System.Boolean ADD2, bool ignore_ADD2, System.Boolean ADD3, bool ignore_ADD3, System.UInt16 Xpos, bool ignore_Xpos, System.UInt16 Ypos, bool ignore_Ypos)
    {
      object[] _values_ = new object[5];
      if (!ignore_ADD1) _values_[0] = ADD1;
      if (!ignore_ADD2) _values_[1] = ADD2;
      if (!ignore_ADD3) _values_[2] = ADD3;
      if (!ignore_Xpos) _values_[3] = Xpos;
      if (!ignore_Ypos) _values_[4] = Ypos;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode)
    {
      return ((IHMIAccessorOutput)this).FireEvent(1, new object[] {Mode});
    }
    public bool FireEvent_MODE(HMI.Main.Symbols.PnPView.MODEEventArgs ea)
    {
      object[] _values_ = new object[1];
      if (ea.Mode.HasValue) _values_[0] = ea.Mode.Value;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_MODE(System.SByte Mode, bool ignore_Mode)
    {
      object[] _values_ = new object[1];
      if (!ignore_Mode) _values_[0] = Mode;
      return ((IHMIAccessorOutput)this).FireEvent(1, _values_);
    }
    public bool FireEvent_ESTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(2, new object[] {});
    }
    public bool FireEvent_ESTOP(HMI.Main.Symbols.PnPView.ESTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(2, _values_);
    }
    public bool FireEvent_HISTOP()
    {
      return ((IHMIAccessorOutput)this).FireEvent(3, new object[] {});
    }
    public bool FireEvent_HISTOP(HMI.Main.Symbols.PnPView.HISTOPEventArgs ea)
    {
      object[] _values_ = new object[0];
      return ((IHMIAccessorOutput)this).FireEvent(3, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, System.Boolean fCYL2, System.Boolean fCYL3, System.Boolean fVCYL1, System.Boolean fVCYL2, System.Boolean fVCYL3)
    {
      return ((IHMIAccessorOutput)this).FireEvent(4, new object[] {fCYL1, fCYL2, fCYL3, fVCYL1, fVCYL2, fVCYL3});
    }
    public bool FireEvent_FAULT(HMI.Main.Symbols.PnPView.FAULTEventArgs ea)
    {
      object[] _values_ = new object[6];
      if (ea.fCYL1.HasValue) _values_[0] = ea.fCYL1.Value;
      if (ea.fCYL2.HasValue) _values_[1] = ea.fCYL2.Value;
      if (ea.fCYL3.HasValue) _values_[2] = ea.fCYL3.Value;
      if (ea.fVCYL1.HasValue) _values_[3] = ea.fVCYL1.Value;
      if (ea.fVCYL2.HasValue) _values_[4] = ea.fVCYL2.Value;
      if (ea.fVCYL3.HasValue) _values_[5] = ea.fVCYL3.Value;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }
    public bool FireEvent_FAULT(System.Boolean fCYL1, bool ignore_fCYL1, System.Boolean fCYL2, bool ignore_fCYL2, System.Boolean fCYL3, bool ignore_fCYL3, System.Boolean fVCYL1, bool ignore_fVCYL1, System.Boolean fVCYL2, bool ignore_fVCYL2, System.Boolean fVCYL3, bool ignore_fVCYL3)
    {
      object[] _values_ = new object[6];
      if (!ignore_fCYL1) _values_[0] = fCYL1;
      if (!ignore_fCYL2) _values_[1] = fCYL2;
      if (!ignore_fCYL3) _values_[2] = fCYL3;
      if (!ignore_fVCYL1) _values_[3] = fVCYL1;
      if (!ignore_fVCYL2) _values_[4] = fVCYL2;
      if (!ignore_fVCYL3) _values_[5] = fVCYL3;
      return ((IHMIAccessorOutput)this).FireEvent(4, _values_);
    }

  }
}
#endregion #PnPView_HMI;

#endregion Definitions;
