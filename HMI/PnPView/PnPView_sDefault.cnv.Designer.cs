﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/5/2014
 * Time: 8:25 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Collections;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.PnPView
{
	/// <summary>
	/// Summary description for sDefault.
	/// </summary>
	partial class sDefault
	{

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sDefault));
			this.pnpVC1p = new NxtControl.GuiFramework.Rectangle();
			this.pnpVC1c = new NxtControl.GuiFramework.Rectangle();
			this.pnpVC2p = new NxtControl.GuiFramework.Rectangle();
			this.pnpVC2c = new NxtControl.GuiFramework.Rectangle();
			this.pnpVC3 = new NxtControl.GuiFramework.Rectangle();
			this.pp8 = new NxtControl.GuiFramework.Rectangle();
			this.pp1 = new NxtControl.GuiFramework.Rectangle();
			this.pp6 = new NxtControl.GuiFramework.Rectangle();
			this.wp3 = new NxtControl.GuiFramework.Rectangle();
			this.wp2 = new NxtControl.GuiFramework.Rectangle();
			this.wp1 = new NxtControl.GuiFramework.Rectangle();
			this.pnpVC2 = new NxtControl.GuiFramework.Group();
			this.pnpVC1 = new NxtControl.GuiFramework.Group();
			this.pnpRC = new NxtControl.GuiFramework.Rectangle();
			this.pnpMC = new NxtControl.GuiFramework.Rectangle();
			this.vac = new NxtControl.GuiFramework.Rectangle();
			this.pnpLC = new NxtControl.GuiFramework.Rectangle();
			this.pnpBase = new NxtControl.GuiFramework.Rectangle();
			this.Slider = new NxtControl.GuiFramework.Rectangle();
			this.pp12 = new NxtControl.GuiFramework.Rectangle();
			this.pp4 = new NxtControl.GuiFramework.Rectangle();
			this.pp7 = new NxtControl.GuiFramework.Rectangle();
			this.pp10 = new NxtControl.GuiFramework.Rectangle();
			this.pp11 = new NxtControl.GuiFramework.Rectangle();
			this.pp5 = new NxtControl.GuiFramework.Rectangle();
			this.pp2 = new NxtControl.GuiFramework.Rectangle();
			this.pp3 = new NxtControl.GuiFramework.Rectangle();
			this.pp9 = new NxtControl.GuiFramework.Rectangle();
			this.rectangle1 = new NxtControl.GuiFramework.Rectangle();
			this.pp13 = new NxtControl.GuiFramework.Rectangle();
			this.pp14 = new NxtControl.GuiFramework.Rectangle();
			this.pp15 = new NxtControl.GuiFramework.Rectangle();
			this.pp16 = new NxtControl.GuiFramework.Rectangle();
			this.pp17 = new NxtControl.GuiFramework.Rectangle();
			this.pp18 = new NxtControl.GuiFramework.Rectangle();
			this.pp19 = new NxtControl.GuiFramework.Rectangle();
			this.pp20 = new NxtControl.GuiFramework.Rectangle();
			this.CYL1Fault = new NxtControl.GuiFramework.Rectangle();
			this.CYL2Fault = new NxtControl.GuiFramework.Rectangle();
			this.CYL3Fault = new NxtControl.GuiFramework.Rectangle();
			this.VCYL1Fault = new NxtControl.GuiFramework.Rectangle();
			this.VCYL2Fault = new NxtControl.GuiFramework.Rectangle();
			this.VCYL3Fault = new NxtControl.GuiFramework.Rectangle();
			// 
			// pnpVC1p
			// 
			this.pnpVC1p.Bounds = new NxtControl.Drawing.RectF(((float)(392D)), ((float)(40D)), ((float)(60D)), ((float)(90D)));
			this.pnpVC1p.Brush = new NxtControl.Drawing.Brush();
			this.pnpVC1p.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pnpVC1p.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pnpVC1p.ImageStream")));
			this.pnpVC1p.Name = "pnpVC1p";
			this.pnpVC1p.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// pnpVC1c
			// 
			this.pnpVC1c.Bounds = new NxtControl.Drawing.RectF(((float)(392D)), ((float)(126D)), ((float)(60D)), ((float)(92D)));
			this.pnpVC1c.Brush = new NxtControl.Drawing.Brush();
			this.pnpVC1c.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pnpVC1c.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pnpVC1c.ImageStream")));
			this.pnpVC1c.Name = "pnpVC1c";
			this.pnpVC1c.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// pnpVC2p
			// 
			this.pnpVC2p.Bounds = new NxtControl.Drawing.RectF(((float)(392D)), ((float)(128D)), ((float)(60D)), ((float)(90D)));
			this.pnpVC2p.Brush = new NxtControl.Drawing.Brush();
			this.pnpVC2p.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pnpVC2p.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pnpVC2p.ImageStream")));
			this.pnpVC2p.Name = "pnpVC2p";
			this.pnpVC2p.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// pnpVC2c
			// 
			this.pnpVC2c.Bounds = new NxtControl.Drawing.RectF(((float)(392D)), ((float)(214D)), ((float)(60D)), ((float)(92D)));
			this.pnpVC2c.Brush = new NxtControl.Drawing.Brush();
			this.pnpVC2c.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pnpVC2c.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pnpVC2c.ImageStream")));
			this.pnpVC2c.Name = "pnpVC2c";
			this.pnpVC2c.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// pnpVC3
			// 
			this.pnpVC3.Bounds = new NxtControl.Drawing.RectF(((float)(399D)), ((float)(223D)), ((float)(26D)), ((float)(101D)));
			this.pnpVC3.Brush = new NxtControl.Drawing.Brush();
			this.pnpVC3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pnpVC3.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pnpVC3.ImageStream")));
			this.pnpVC3.Name = "pnpVC3";
			this.pnpVC3.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// pp8
			// 
			this.pp8.Bounds = new NxtControl.Drawing.RectF(((float)(616D)), ((float)(482D)), ((float)(44D)), ((float)(13D)));
			this.pp8.Brush = new NxtControl.Drawing.Brush();
			this.pp8.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp8.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp8.ImageStream")));
			this.pp8.Name = "pp8";
			this.pp8.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp8.Click += new System.EventHandler(this.Pp8Click);
			// 
			// pp1
			// 
			this.pp1.Bounds = new NxtControl.Drawing.RectF(((float)(466D)), ((float)(407D)), ((float)(44D)), ((float)(13D)));
			this.pp1.Brush = new NxtControl.Drawing.Brush();
			this.pp1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp1.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp1.ImageStream")));
			this.pp1.Name = "pp1";
			this.pp1.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp1.Click += new System.EventHandler(this.Pp1Click);
			// 
			// pp6
			// 
			this.pp6.Bounds = new NxtControl.Drawing.RectF(((float)(541D)), ((float)(557D)), ((float)(44D)), ((float)(13D)));
			this.pp6.Brush = new NxtControl.Drawing.Brush();
			this.pp6.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp6.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp6.ImageStream")));
			this.pp6.Name = "pp6";
			this.pp6.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp6.Click += new System.EventHandler(this.Pp6Click);
			// 
			// wp3
			// 
			this.wp3.Bounds = new NxtControl.Drawing.RectF(((float)(760D)), ((float)(88D)), ((float)(36D)), ((float)(15D)));
			this.wp3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.wp3.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("wp3.ImageStream")));
			this.wp3.Name = "wp3";
			// 
			// wp2
			// 
			this.wp2.Bounds = new NxtControl.Drawing.RectF(((float)(758D)), ((float)(143D)), ((float)(36D)), ((float)(15D)));
			this.wp2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.wp2.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("wp2.ImageStream")));
			this.wp2.Name = "wp2";
			// 
			// wp1
			// 
			this.wp1.Bounds = new NxtControl.Drawing.RectF(((float)(759D)), ((float)(119D)), ((float)(36D)), ((float)(15D)));
			this.wp1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.wp1.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("wp1.ImageStream")));
			this.wp1.Name = "wp1";
			// 
			// pnpVC2
			// 
			this.pnpVC2.BeginInit();
			this.pnpVC2.Name = "pnpVC2";
			this.pnpVC2.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.pnpVC2p,
									this.pnpVC2c});
			this.pnpVC2.EndInit();
			// 
			// pnpVC1
			// 
			this.pnpVC1.BeginInit();
			this.pnpVC1.Name = "pnpVC1";
			this.pnpVC1.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.pnpVC1p,
									this.pnpVC1c});
			this.pnpVC1.EndInit();
			// 
			// pnpRC
			// 
			this.pnpRC.Bounds = new NxtControl.Drawing.RectF(((float)(213D)), ((float)(32D)), ((float)(242D)), ((float)(94D)));
			this.pnpRC.Brush = new NxtControl.Drawing.Brush();
			this.pnpRC.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pnpRC.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pnpRC.ImageStream")));
			this.pnpRC.Name = "pnpRC";
			this.pnpRC.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// pnpMC
			// 
			this.pnpMC.Bounds = new NxtControl.Drawing.RectF(((float)(118D)), ((float)(23D)), ((float)(273D)), ((float)(72D)));
			this.pnpMC.Brush = new NxtControl.Drawing.Brush();
			this.pnpMC.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pnpMC.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pnpMC.ImageStream")));
			this.pnpMC.Name = "pnpMC";
			this.pnpMC.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// vac
			// 
			this.vac.Bounds = new NxtControl.Drawing.RectF(((float)(728D)), ((float)(52D)), ((float)(26D)), ((float)(91D)));
			this.vac.Brush = new NxtControl.Drawing.Brush();
			this.vac.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.vac.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("vac.ImageStream")));
			this.vac.Name = "vac";
			this.vac.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color("Transparent"), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.vac.Visible = false;
			// 
			// pnpLC
			// 
			this.pnpLC.Bounds = new NxtControl.Drawing.RectF(((float)(24D)), ((float)(24D)), ((float)(183D)), ((float)(72D)));
			this.pnpLC.Brush = new NxtControl.Drawing.Brush();
			this.pnpLC.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pnpLC.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pnpLC.ImageStream")));
			this.pnpLC.Name = "pnpLC";
			this.pnpLC.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// pnpBase
			// 
			this.pnpBase.Bounds = new NxtControl.Drawing.RectF(((float)(0D)), ((float)(24D)), ((float)(112D)), ((float)(205D)));
			this.pnpBase.Brush = new NxtControl.Drawing.Brush();
			this.pnpBase.FillDirection = NxtControl.Drawing.FillDirection.TopToDown;
			this.pnpBase.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pnpBase.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pnpBase.ImageStream")));
			this.pnpBase.Name = "pnpBase";
			this.pnpBase.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 0F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// Slider
			// 
			this.Slider.Bounds = new NxtControl.Drawing.RectF(((float)(391D)), ((float)(395D)), ((float)(44D)), ((float)(126D)));
			this.Slider.Brush = new NxtControl.Drawing.Brush();
			this.Slider.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.Slider.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("Slider.ImageStream")));
			this.Slider.Name = "Slider";
			this.Slider.OpenFaceplates.Add(new NxtControl.GuiFramework.OpenFaceplate("Faceplate", NxtControl.GuiFramework.MouseButtonType.DoubleClick));
			this.Slider.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			// 
			// pp12
			// 
			this.pp12.Bounds = new NxtControl.Drawing.RectF(((float)(691D)), ((float)(557D)), ((float)(44D)), ((float)(13D)));
			this.pp12.Brush = new NxtControl.Drawing.Brush();
			this.pp12.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp12.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp12.ImageStream")));
			this.pp12.Name = "pp12";
			this.pp12.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp12.Click += new System.EventHandler(this.Pp12Click);
			// 
			// pp4
			// 
			this.pp4.Bounds = new NxtControl.Drawing.RectF(((float)(541D)), ((float)(407D)), ((float)(44D)), ((float)(13D)));
			this.pp4.Brush = new NxtControl.Drawing.Brush();
			this.pp4.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp4.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp4.ImageStream")));
			this.pp4.Name = "pp4";
			this.pp4.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp4.Click += new System.EventHandler(this.Pp4Click);
			// 
			// pp7
			// 
			this.pp7.Bounds = new NxtControl.Drawing.RectF(((float)(616D)), ((float)(407D)), ((float)(44D)), ((float)(13D)));
			this.pp7.Brush = new NxtControl.Drawing.Brush();
			this.pp7.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp7.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp7.ImageStream")));
			this.pp7.Name = "pp7";
			this.pp7.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp7.Click += new System.EventHandler(this.Pp7Click);
			// 
			// pp10
			// 
			this.pp10.Bounds = new NxtControl.Drawing.RectF(((float)(691D)), ((float)(407D)), ((float)(44D)), ((float)(13D)));
			this.pp10.Brush = new NxtControl.Drawing.Brush();
			this.pp10.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp10.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp10.ImageStream")));
			this.pp10.Name = "pp10";
			this.pp10.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp10.Click += new System.EventHandler(this.Pp10Click);
			// 
			// pp11
			// 
			this.pp11.Bounds = new NxtControl.Drawing.RectF(((float)(691D)), ((float)(482D)), ((float)(44D)), ((float)(13D)));
			this.pp11.Brush = new NxtControl.Drawing.Brush();
			this.pp11.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp11.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp11.ImageStream")));
			this.pp11.Name = "pp11";
			this.pp11.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp11.Click += new System.EventHandler(this.Pp11Click);
			// 
			// pp5
			// 
			this.pp5.Bounds = new NxtControl.Drawing.RectF(((float)(541D)), ((float)(482D)), ((float)(44D)), ((float)(13D)));
			this.pp5.Brush = new NxtControl.Drawing.Brush();
			this.pp5.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp5.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp5.ImageStream")));
			this.pp5.Name = "pp5";
			this.pp5.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp5.Click += new System.EventHandler(this.Pp5Click);
			// 
			// pp2
			// 
			this.pp2.Bounds = new NxtControl.Drawing.RectF(((float)(466D)), ((float)(482D)), ((float)(44D)), ((float)(13D)));
			this.pp2.Brush = new NxtControl.Drawing.Brush();
			this.pp2.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp2.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp2.ImageStream")));
			this.pp2.Name = "pp2";
			this.pp2.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp2.Click += new System.EventHandler(this.Pp2Click);
			// 
			// pp3
			// 
			this.pp3.Bounds = new NxtControl.Drawing.RectF(((float)(466D)), ((float)(557D)), ((float)(44D)), ((float)(13D)));
			this.pp3.Brush = new NxtControl.Drawing.Brush();
			this.pp3.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp3.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp3.ImageStream")));
			this.pp3.Name = "pp3";
			this.pp3.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp3.Click += new System.EventHandler(this.Pp3Click);
			// 
			// pp9
			// 
			this.pp9.Bounds = new NxtControl.Drawing.RectF(((float)(618D)), ((float)(557D)), ((float)(44D)), ((float)(13D)));
			this.pp9.Brush = new NxtControl.Drawing.Brush();
			this.pp9.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp9.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp9.ImageStream")));
			this.pp9.Name = "pp9";
			this.pp9.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp9.Click += new System.EventHandler(this.Pp9Click);
			// 
			// rectangle1
			// 
			this.rectangle1.Bounds = new NxtControl.Drawing.RectF(((float)(-50D)), ((float)(-4D)), ((float)(972D)), ((float)(709D)));
			this.rectangle1.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.rectangle1.Name = "rectangle1";
			// 
			// pp13
			// 
			this.pp13.Bounds = new NxtControl.Drawing.RectF(((float)(766D)), ((float)(407D)), ((float)(44D)), ((float)(13D)));
			this.pp13.Brush = new NxtControl.Drawing.Brush();
			this.pp13.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp13.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp13.ImageStream")));
			this.pp13.Name = "pp13";
			this.pp13.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp13.Click += new System.EventHandler(this.Pp13Click);
			// 
			// pp14
			// 
			this.pp14.Bounds = new NxtControl.Drawing.RectF(((float)(766D)), ((float)(482D)), ((float)(44D)), ((float)(13D)));
			this.pp14.Brush = new NxtControl.Drawing.Brush();
			this.pp14.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp14.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp14.ImageStream")));
			this.pp14.Name = "pp14";
			this.pp14.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp14.Click += new System.EventHandler(this.Pp14Click);
			// 
			// pp15
			// 
			this.pp15.Bounds = new NxtControl.Drawing.RectF(((float)(766D)), ((float)(557D)), ((float)(44D)), ((float)(13D)));
			this.pp15.Brush = new NxtControl.Drawing.Brush();
			this.pp15.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp15.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp15.ImageStream")));
			this.pp15.Name = "pp15";
			this.pp15.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp15.Click += new System.EventHandler(this.Pp15Click);
			// 
			// pp16
			// 
			this.pp16.Bounds = new NxtControl.Drawing.RectF(((float)(466D)), ((float)(632D)), ((float)(44D)), ((float)(13D)));
			this.pp16.Brush = new NxtControl.Drawing.Brush();
			this.pp16.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp16.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp16.ImageStream")));
			this.pp16.Name = "pp16";
			this.pp16.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp16.Click += new System.EventHandler(this.Pp16Click);
			// 
			// pp17
			// 
			this.pp17.Bounds = new NxtControl.Drawing.RectF(((float)(541D)), ((float)(632D)), ((float)(44D)), ((float)(13D)));
			this.pp17.Brush = new NxtControl.Drawing.Brush();
			this.pp17.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp17.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp17.ImageStream")));
			this.pp17.Name = "pp17";
			this.pp17.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp17.Click += new System.EventHandler(this.Pp17Click);
			// 
			// pp18
			// 
			this.pp18.Bounds = new NxtControl.Drawing.RectF(((float)(618D)), ((float)(632D)), ((float)(44D)), ((float)(13D)));
			this.pp18.Brush = new NxtControl.Drawing.Brush();
			this.pp18.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp18.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp18.ImageStream")));
			this.pp18.Name = "pp18";
			this.pp18.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp18.Click += new System.EventHandler(this.Pp18Click);
			// 
			// pp19
			// 
			this.pp19.Bounds = new NxtControl.Drawing.RectF(((float)(691D)), ((float)(632D)), ((float)(44D)), ((float)(13D)));
			this.pp19.Brush = new NxtControl.Drawing.Brush();
			this.pp19.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp19.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp19.ImageStream")));
			this.pp19.Name = "pp19";
			this.pp19.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp19.Click += new System.EventHandler(this.Pp19Click);
			// 
			// pp20
			// 
			this.pp20.Bounds = new NxtControl.Drawing.RectF(((float)(766D)), ((float)(632D)), ((float)(44D)), ((float)(13D)));
			this.pp20.Brush = new NxtControl.Drawing.Brush();
			this.pp20.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.pp20.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("pp20.ImageStream")));
			this.pp20.Name = "pp20";
			this.pp20.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(255)), ((byte)(255)), ((byte)(255))), 1F, NxtControl.Drawing.DashStyle.Solid);
			this.pp20.Click += new System.EventHandler(this.Pp20Click);
			// 
			// CYL1Fault
			// 
			this.CYL1Fault.Bounds = new NxtControl.Drawing.RectF(((float)(42D)), ((float)(35D)), ((float)(38D)), ((float)(34D)));
			this.CYL1Fault.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.CYL1Fault.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.CYL1Fault.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("CYL1Fault.ImageStream")));
			this.CYL1Fault.Name = "CYL1Fault";
			this.CYL1Fault.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.CYL1Fault.Visible = false;
			// 
			// CYL2Fault
			// 
			this.CYL2Fault.Bounds = new NxtControl.Drawing.RectF(((float)(138D)), ((float)(36D)), ((float)(38D)), ((float)(34D)));
			this.CYL2Fault.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.CYL2Fault.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.CYL2Fault.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("CYL2Fault.ImageStream")));
			this.CYL2Fault.Name = "CYL2Fault";
			this.CYL2Fault.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.CYL2Fault.Visible = false;
			// 
			// CYL3Fault
			// 
			this.CYL3Fault.Bounds = new NxtControl.Drawing.RectF(((float)(275D)), ((float)(36D)), ((float)(38D)), ((float)(34D)));
			this.CYL3Fault.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.CYL3Fault.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.CYL3Fault.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("CYL3Fault.ImageStream")));
			this.CYL3Fault.Name = "CYL3Fault";
			this.CYL3Fault.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.CYL3Fault.Visible = false;
			// 
			// VCYL1Fault
			// 
			this.VCYL1Fault.Bounds = new NxtControl.Drawing.RectF(((float)(393D)), ((float)(63D)), ((float)(38D)), ((float)(34D)));
			this.VCYL1Fault.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.VCYL1Fault.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.VCYL1Fault.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("VCYL1Fault.ImageStream")));
			this.VCYL1Fault.Name = "VCYL1Fault";
			this.VCYL1Fault.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.VCYL1Fault.Visible = false;
			// 
			// VCYL2Fault
			// 
			this.VCYL2Fault.Bounds = new NxtControl.Drawing.RectF(((float)(394D)), ((float)(151D)), ((float)(38D)), ((float)(34D)));
			this.VCYL2Fault.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.VCYL2Fault.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.VCYL2Fault.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("VCYL2Fault.ImageStream")));
			this.VCYL2Fault.Name = "VCYL2Fault";
			this.VCYL2Fault.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.VCYL2Fault.Visible = false;
			// 
			// VCYL3Fault
			// 
			this.VCYL3Fault.Bounds = new NxtControl.Drawing.RectF(((float)(392D)), ((float)(243D)), ((float)(38D)), ((float)(34D)));
			this.VCYL3Fault.Brush = new NxtControl.Drawing.Brush(new NxtControl.Drawing.Color("Transparent"));
			this.VCYL3Fault.Font = new NxtControl.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
			this.VCYL3Fault.ImageStream = ((System.IO.MemoryStream)(resources.GetObject("VCYL3Fault.ImageStream")));
			this.VCYL3Fault.Name = "VCYL3Fault";
			this.VCYL3Fault.Pen = new NxtControl.Drawing.Pen(new NxtControl.Drawing.Color(((byte)(0)), ((byte)(0)), ((byte)(0))), 0F, NxtControl.Drawing.DashStyle.Solid);
			this.VCYL3Fault.Visible = false;
			// 
			// sDefault
			// 
			this.Name = "sDefault";
			this.Shapes.AddRange(new System.ComponentModel.IComponent[] {
									this.rectangle1,
									this.Slider,
									this.wp3,
									this.pnpVC1,
									this.pnpRC,
									this.pnpMC,
									this.vac,
									this.pnpLC,
									this.pnpBase,
									this.pp1,
									this.pp2,
									this.pp3,
									this.pp5,
									this.pp4,
									this.pp6,
									this.pp7,
									this.pp8,
									this.pp9,
									this.pp10,
									this.pp11,
									this.pp12,
									this.pp13,
									this.pp14,
									this.pp15,
									this.pp16,
									this.pp17,
									this.pp18,
									this.pp19,
									this.pp20,
									this.pnpVC3,
									this.pnpVC2,
									this.pnpVC1c,
									this.wp2,
									this.wp1,
									this.CYL1Fault,
									this.CYL2Fault,
									this.CYL3Fault,
									this.VCYL1Fault,
									this.VCYL2Fault,
									this.VCYL3Fault});
			this.SymbolSize = new System.Drawing.Size(896, 603);
		}
		private NxtControl.GuiFramework.Rectangle VCYL3Fault;
		private NxtControl.GuiFramework.Rectangle VCYL2Fault;
		private NxtControl.GuiFramework.Rectangle VCYL1Fault;
		private NxtControl.GuiFramework.Rectangle CYL3Fault;
		private NxtControl.GuiFramework.Rectangle CYL2Fault;
		private NxtControl.GuiFramework.Rectangle CYL1Fault;
		private NxtControl.GuiFramework.Rectangle pp20;
		private NxtControl.GuiFramework.Rectangle pp19;
		private NxtControl.GuiFramework.Rectangle pp18;
		private NxtControl.GuiFramework.Rectangle pp17;
		private NxtControl.GuiFramework.Rectangle pp16;
		private NxtControl.GuiFramework.Rectangle pp15;
		private NxtControl.GuiFramework.Rectangle pp14;
		private NxtControl.GuiFramework.Rectangle pp13;
		private NxtControl.GuiFramework.Rectangle rectangle1;
		private NxtControl.GuiFramework.Rectangle pp8;
		private NxtControl.GuiFramework.Rectangle pp6;
		private NxtControl.GuiFramework.Rectangle pp12;
		private NxtControl.GuiFramework.Rectangle pp4;
		private NxtControl.GuiFramework.Rectangle pp7;
		private NxtControl.GuiFramework.Rectangle pp10;
		private NxtControl.GuiFramework.Rectangle pp11;
		private NxtControl.GuiFramework.Rectangle pp5;
		private NxtControl.GuiFramework.Rectangle pp9;
		private NxtControl.GuiFramework.Rectangle Slider;
		private NxtControl.GuiFramework.Rectangle pnpBase;
		private NxtControl.GuiFramework.Rectangle pnpLC;
		private NxtControl.GuiFramework.Rectangle vac;
		private NxtControl.GuiFramework.Rectangle pnpMC;
		private NxtControl.GuiFramework.Rectangle pnpRC;
		private NxtControl.GuiFramework.Group pnpVC1;
		private NxtControl.GuiFramework.Group pnpVC2;
		private NxtControl.GuiFramework.Rectangle wp1;
		private NxtControl.GuiFramework.Rectangle wp2;
		private NxtControl.GuiFramework.Rectangle wp3;
		private NxtControl.GuiFramework.Rectangle pp3;
		private NxtControl.GuiFramework.Rectangle pp2;
		private NxtControl.GuiFramework.Rectangle pp1;
		private NxtControl.GuiFramework.Rectangle pnpVC3;
		private NxtControl.GuiFramework.Rectangle pnpVC2c;
		private NxtControl.GuiFramework.Rectangle pnpVC2p;
		private NxtControl.GuiFramework.Rectangle pnpVC1c;
		private NxtControl.GuiFramework.Rectangle pnpVC1p;
		#endregion
	}
}
