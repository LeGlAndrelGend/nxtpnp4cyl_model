﻿/*
 * Created by nxtSTUDIO.
 * User: patsan
 * Date: 2/5/2014
 * Time: 8:25 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.PnPView
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
	  ushort BASE_X;
    ushort BASE_Y;
    ushort CYL1_X;
    ushort CYL1_Y;
    ushort CYL2_X;
    ushort CYL2_Y;
    ushort CYL3_X;
    ushort CYL3_Y;
    ushort VCYL1_X;
    ushort VCYL1_Y;
    ushort VCYL2_X;
    ushort VCYL2_Y;
    ushort VCYL3_X;
    ushort VCYL3_Y;
    ushort WP1_X;
    ushort WP1_Y;
    ushort WP2_X;
    ushort WP2_Y;
    ushort WP3_X;
    ushort WP3_Y;
    bool VACUUM_ON;
    ushort vc1y;
    ushort vc2y;
		public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			this.REQ_Fired += new EventHandler<REQEventArgs>(onReq);
			
			vc1y = (ushort)90;
		  vc2y = (ushort)90;
		}
		
		void onReq(object sender, REQEventArgs ea)
    {  
		  //MessageBox.Show("OnREQ", "Req Yayy");		    
		  ea.Get_BASE_X(ref BASE_X);
		  ea.Get_BASE_Y(ref BASE_Y);
		  ea.Get_CYL1_X(ref CYL1_X);
		  ea.Get_CYL1_Y(ref CYL1_Y);
		  ea.Get_CYL2_X(ref CYL2_X);
		  ea.Get_CYL2_Y(ref CYL2_Y);
		  ea.Get_CYL3_X(ref CYL3_X);
		  ea.Get_CYL3_Y(ref CYL3_Y);
		  ea.Get_VCYL1_X(ref VCYL1_X);
		  ea.Get_VCYL1_Y(ref VCYL1_Y);
		  ea.Get_VCYL2_X(ref VCYL2_X);
		  ea.Get_VCYL2_Y(ref VCYL2_Y);
		  ea.Get_VCYL3_X(ref VCYL3_X);
		  ea.Get_VCYL3_Y(ref VCYL3_Y);
		  ea.Get_WP1_X(ref WP1_X);
		  ea.Get_WP1_Y(ref WP1_Y);
		  ea.Get_WP2_X(ref WP2_X);
		  ea.Get_WP2_Y(ref WP2_Y);
		  ea.Get_WP3_X(ref WP3_X);
		  ea.Get_WP3_Y(ref WP3_Y);
		  ea.Get_VACUUM_ON(ref VACUUM_ON);
		  
		  pnpLC.X = CYL1_X;
		  pnpMC.X = CYL2_X;
		  pnpRC.X = CYL3_X;
		  pnpVC1p.X = VCYL1_X;
		  pnpVC1p.Y = VCYL1_Y;
		  pnpVC2p.X = VCYL2_X;
		  pnpVC2p.Y = VCYL2_Y;
		  pnpVC1c.X = VCYL1_X;
		  pnpVC1c.Y = vc1y + VCYL1_Y;
		  pnpVC2c.X = VCYL2_X;
		  pnpVC2c.Y = vc2y + VCYL2_Y;
		  pnpVC3.X = VCYL3_X;
		  pnpVC3.Y = VCYL3_Y;
		  vac.X = VCYL3_X;
		  vac.Y = VCYL3_Y;
		  wp1.X = WP1_X;
		  wp1.Y = WP1_Y;
		  wp2.X = WP2_X;
		  wp2.Y = WP2_Y;
		  wp3.X = WP3_X;
		  wp3.Y = WP3_Y;
		  if(VACUUM_ON)
		    vac.Visible = true;
		  else
		    vac.Visible = false;
    }
		
		void Pp1Click(object sender, EventArgs e)
		{
		  //System.Windows.Forms.MessageBox.Show("pp1 clicked");
		  this.FireEvent_CNF(true, false, false, 75, 75);
		}
		
		void Pp2Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 75, 150);
		}
		
		void Pp3Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 75, 225);
		}
		
		void Pp4Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 150, 75);
		}
		
		void Pp5Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 150, 150);
		}
		
		void Pp6Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 150, 225);
		}
		
		void Pp7Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 225, 75);
		}
		
		void Pp8Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 225, 150);
		}
		
		void Pp9Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 225, 225);
		}	
		
		void Pp10Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 300, 75);
		}
		
		void Pp11Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 300, 150);
		}
		
		void Pp12Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 300, 225);
		}
		
		void Pp13Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 375, 75);
		}
		
		void Pp14Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 375, 150);
		}
		
		void Pp15Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 375, 225);
		}
		
		void Pp16Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 75, 300);
		}
		
		void Pp17Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 150, 300);
		}
		
		void Pp18Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 225, 300);
		}
		
		void Pp19Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 300, 300);
		}
		
		void Pp20Click(object sender, EventArgs e)
		{
			this.FireEvent_CNF(true, false, false, 375, 300);
		}
	}
}
